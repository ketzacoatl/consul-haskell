# Inspired by https://discourse.nixos.org/t/nix-haskell-development-2020/6170/16
let
  pinnedPkgs = builtins.fetchTarball {
    # Descriptive name to make the store path easier to identify
    name = "nixos-23.11-2023-12-22";
    # Current commit from https://github.com/NixOS/nixpkgs/tree/nixos-23.11
    url = "https://github.com/nixos/nixpkgs/archive/d65bceaee0fb1e64363f7871bc43dc1c6ecad99f.tar.gz";
    # Hash obtained using `nix-prefetch-url --unpack <url>`
    sha256 = "0q5dhs9nxg6yzghw5nnj3py479gfdxg9xrc7f16bxhx5m8akjb2r";
  };
in
{
  nixpkgs ? pinnedPkgs,
}:
let
  pkgs = import pinnedPkgs {
    overlays = [
      (import ./overlay.nix)
    ];
  };
in pkgs.haskellPackages.consul-haskell
