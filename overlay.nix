final: prev:

let
  explicitSource =
    (import ./explicitSource.nix { lib = final.lib; }).explicitSource;

  src = explicitSource ./. {
    name = "consul-haskell";
    includeDirs = [
      ./src
      ./tests ];
    includeFiles = [
      ./consul-haskell.cabal
      ./Setup.hs
      ./LICENSE
      ./README.md
    ];
    pathComponentExcludes = [ "build" "gen" ];
  };
in
{
  haskellPackages = prev.haskellPackages.override (old: {
    overrides = final.lib.composeExtensions (old.overrides or (_: _: { })) (
      self: super: {
        consul-haskell =
          final.haskell.lib.addTestToolDepend
            (self.callCabal2nix "consul-haskell" src {})
            final.consul; # Add consul executable from nixpkgs
      }
    );
  });
}



