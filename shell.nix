let
  # Pin nixpkgs.
  # mpickering's NUR used below currently requires 20.03,
  # see https://github.com/mpickering/old-ghc-nix/issues/8.
  pkgs = import (builtins.fetchTarball {
    # Descriptive name to make the store path easier to identify
    name = "nixos-23.11-2023-12-22";
    # Current commit from https://github.com/NixOS/nixpkgs/tree/nixos-23.11
    url = "https://github.com/nixos/nixpkgs/archive/d65bceaee0fb1e64363f7871bc43dc1c6ecad99f.tar.gz";
    # Hash obtained using `nix-prefetch-url --unpack <url>`
    sha256 = "0q5dhs9nxg6yzghw5nnj3py479gfdxg9xrc7f16bxhx5m8akjb2r";
  }) {};

  # Needs NUR from https://github.com/nix-community/NUR
  ghc = pkgs.nur.repos.mpickering.ghc.ghc865; # Keep in sync with the GHC version defined by stack.yaml!
in
  pkgs.haskell.lib.buildStackProject {
    inherit ghc;
    name = "myEnv";

    nativeBuildInputs = with pkgs; [
      # Put libraries required to build here.
      zlib

      # Executables needed to run tests:
      consul
    ];
  }
